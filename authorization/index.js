

const jwt = require("jsonwebtoken");
const cookie = require('cookie');
const pako = require('pako');


module.exports = class {
  static async construct(auth, aura, cfg) {
    try {
      let _this = new module.exports(auth, cfg);
      

      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(auth, cfg) {
    this.auth = auth;

    this.paths = cfg.auth_paths;

    this.forward_token = cfg.forward_token; // TODO CHECK THIS

    this.name = cfg.table_name || "user_accounts";

    this.prefix = cfg.prefix;

  }


  async attach_session(req, res, next, required_rights) {
    try {
//      console.log("\x1b[32m"+"FLYATTACH SESSION", "\x1b[0m"+req.originalUrl);
      const { accepts_html } = this.parse_req_info(req);
//        console.log("\x1b[32m"+"FLYAUTH ATTACH SESSION", "\x1b[0m"+req.originalUrl);
        let auth_err = await this.is_unauthorized(req, required_rights);
//      console.log("AUTH", auth_err);
        if (auth_err) {
//          console.log("terminate", auth_err.terminate);
          if (auth_err.terminate) await this.auth.session.terminate(req, res, next, true);
//          console.log("PASSED");
          if (auth_err.status == 401) {
//            console.log("AUTH_ERR", auth_err);
            if (accepts_html) {
              res.status(auth_err.status).redirect(auth_err.redirect);
            } else {
              res.json({
                err: "UNAUTHORIZED",
                redirect: auth_err.redirect
              });
            }
          } else {
//            console.log("NEXT");
            next();
          }
        } else {
          next();
        }

    } catch (e) {
      console.error(e.stack);
    }
  }


  async authorize(req, res, next, required_rights) {
    try {
//      console.log("\x1b[32m"+"FLYAUTHORIZE", "\x1b[0m"+req.originalUrl);

      let auth_err = await this.is_unauthorized(req, required_rights);
      if (!auth_err) {
        next();
      } else {
        console.log("AUTH_ERR", auth_err);
        if (auth_err.terminate) await this.auth.session.terminate(req, res, next, true);
        if (auth_err.send) {
          res.status(auth_err.status).send(auth_err.send);
        } else if (auth_err.redirect) {
          res.status(auth_err.status).redirect(auth_err.redirect);
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async is_unauthorized(req, required_rights) {
    try {
      const { accepts_html, redirect_path, forbidden_redirect } = this.parse_req_info(req);

      const jwt_token = this.parse_jwt_token(req);
      const jwt_token_exp = this.parse_jwt_token(req, true);

      const authentials = this.parse_authentials(req);

//      console.log("authentials", authentials);

      
      let _this = this;


 //     console.log("jwt_token", jwt_token);
 //     console.log("jwt_token_exp", jwt_token_exp)

      if (!jwt_token && !jwt_token_exp) {
        console.log("\x1b[33m"+"UNAUTH JWT UNDEFINED"+"\x1b[0m");
        if (redirect_path) {
          return {
            status: 403,
            redirect: redirect_path
          }
        } else {
          return {
            status: 403,
            send: "FORBIDDEN" 
          }
        }
      } else {
        let session_data = await this.auth.session.decrypt_token(jwt_token);
//        console.log("SSDATA", session_data);

        const csrf_token = authentials ? authentials.csrf : undefined; 
        const fingerprint = authentials ? JSON.stringify(authentials.fp) : undefined; 

        if ((!session_data/* || csrf_token != session_data.csrfi*/) && jwt_token_exp) {
          session_data = await this.auth.session.decrypt_token(jwt_token_exp);
        }

//        console.log("SSDATA", session_data);
        if (session_data) {
          req.session_data = session_data;

          if (req.originalUrl && req.originalUrl.endsWith(".js.map")) {
            return false;
          } else if (csrf_token && fingerprint) {
            if (session_data.csrf === csrf_token && session_data.fingerprint == fingerprint) {
              if (this.auth.session.check_rights(session_data, required_rights)) {
                return false;
              } else {
                console.log("\x1b[33m"+"UNAUTH insufficient rights"+"\x1b[0m");
                return {
                  status: 403,
                  send: "FORBIDDEN" 
                }
              }

            } else {
              if (session_data.csrf !== csrf_token) {
                console.log("\x1b[33m"+"UNAUTH CSRF"+"\x1b[0m");
                console.debug("DB CSRF", session_data.csrf);
                console.debug("HEAD CSRF", csrf_token);
              }
              if (session_data.fingerprint != fingerprint) {
                console.log("\x1b[33m"+"UNAUTH FP"+"\x1b[0m")
                console.debug("DB FPRINT", JSON.stringify(JSON.parse(session_data.fingerprint), null, 2));
                console.debug("HEAD FPRINT", JSON.stringify(JSON.parse(fingerprint), null, 2));
              }
              if (accepts_html) {
                return {
                  status: 403,
                  redirect: redirect_path,
                  terminate: true
                }
              } else {
                return {
                  status: 403,
                  send: "FORBIDDEN",
                  terminate: true
                }
              }
            }
          } else {
            if (!csrf_token) console.log("\x1b[33m"+"UNAUTH CSRF"+"\x1b[0m");
            if (!fingerprint) console.log("\x1b[33m"+"UNAUTH FP"+"\x1b[0m");
            if (accepts_html) {
              console.log(authentials);
              if (req.session_data && !authentials) {
                req.session_data.unauthOriginalUrl = req.originalUrl;
                return {
                  status: 401,
                  redirect: forbidden_redirect 
                }
              } else {
                return {
                  status: 403,
                  redirect: redirect_path
                }
              }
            } else {
              return {
                status: 401,
                send: "UNAUTHORIZED" 
              }
            }
          }
        } else {
          console.log("JWT", jwt_token);
          console.log("JWT EXP", jwt_token_exp);
          console.log("\x1b[33m"+"UNAUTH SESSION_DATA"+"\x1b[0m")
          if (redirect_path) {
            return {
              status: 403,
              redirect: redirect_path,
              terminate: true
            }
          } else {
            return {
              status: 403,
              send: "FORBIDDEN",
              terminate: true
            }
          }
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  parse_req_info(req) {
    let info = {
      accepts_html: false,
      redirect_path: undefined,
      forbidden_redirect: undefined
    }

    if (req.headers.accept) {
      let request_accepts = req.headers.accept.split(",");
      for (let a = 0; a < request_accepts.length; a++) {
        if (request_accepts[a] === "text/html") info.accepts_html = true;
      }
    }

    if (req.method === "GET") {
      let origurl = req.originalUrl.replace(/(?<=&|\?)authentials(=[^&]*)?(&|$)/g, "");
      if (origurl.endsWith("?")) origurl = origurl.slice(0, -1);
      info.redirect_path = this.paths.unauthorized+"?next_url="+encodeURIComponent(origurl);
      info.forbidden_redirect = "/flyauth-"+this.prefix+"/401/index.html?original_url="+encodeURIComponent(origurl);
    }

    return info;
  }

  parse_jwt_token(req, exp) {
    let cookie_name = this.name+'_access_token';
    if (exp) cookie_name += "_exp";
    if (req.headers.cookie) {
      let cookies = cookie.parse(req.headers.cookie);
      if (cookies[cookie_name]) {
        return cookies[cookie_name];
      }
    }
    return undefined;
  }

  parse_authentials(req) {
    let authentials = undefined;
    if (!req.xhr && req.query) {
      authentials = req.query.authentials;
    } else {
      authentials = req.headers.authorization;
    }

//    console.log("AUTHENTIALS", authentials);
    
    if (authentials) {
      try {
        let uint8agz = Uint8Array.from(Buffer.from(decodeURIComponent(authentials), 'base64'));
//          console.debug(uint8agz);
        let uint8a = pako.inflate(uint8agz);
//          console.debug(uint8a);
        let authentials_hex = Buffer.from(uint8a).toString("hex");
//          console.debug(authentials_hex);
  //        console.debug(authentials_hex);
//        console.log(this.auth.rsa.decrypt);
        authentials = JSON.parse(this.auth.rsa.decrypt(authentials_hex));
    //      console.debug(authentials);
        if (typeof authentials.fp === "string") authentials.fp = JSON.parse(authentials.fp);
        authentials.fp.push(req.get("user-agent"));
        const ip_addr = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
        authentials.fp.push(ip_addr);
//          console.debug(authentials);
      } catch (jsone) {
//        console.debug(jsone.stack);
        authentials = { csrf: undefined, fp: undefined }
      }
    }

    return authentials;
  }
}
