
module.exports = class {
  static async construct() {
    try {
      let _this = new module.exports();
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor() {

  }


  async authenticate(data, req, res, next) {
    var this_class = this;

    function respond(msg, ms) {
      setTimeout(function() {
        res.send(msg);
      }, ms);
    }

    try {
      let fpmod = JSON.parse(data.fp);
      fpmod.push(req.get("user-agent"));
      data.fp = JSON.stringify(fpmod);

      if (!data.email || !data.pwd) {
        res.send(JSON.stringify({
          err: true
        }));
      } else {
        let found = await this.table.select(
          '*', "(email = $1)", [data.email.toLowerCase()]
        );

        if (0 < found.length) {
          found = JSON.parse(JSON.stringify(found[0]));

          let totp_state = this.totp_confirm(found, data);
          if (
            (found.attempts < this.max_attempts || data.security_hash == found.security_hash)
            && bcrypt.compareSync(data.pwd, found.password)
            && data.fp 
            && totp_state
          ) {
            let proceed = true;
         /*   if (this.smtp) {
              if (!found.email_confirmation) {
                proceed = false;
              }
            }*/
            if (proceed) {
              found.next_url = data.next_url;
              let sesres = await this.create_session(found, res, data.fp);
              delete found.password;
              delete found.secret;
              delete found.email_confirmation;
              delete found.security_hash;
              delete found.totp_secret;
              if (this.forward_token) {
                res.json({
                  target: this.paths.authenticated,
                  access_token: sesres.csrf_token
                });
                if (typeof this.on_auth === "function") this.on_auth(found);
              } else {
                res.json({
                  access_token: AES.encrypt(sesres.csrf_token, data.aes_key),
                  next_path: found.super ? "/mellisuga" : this.app.cms.content_manager.path
                });
                if (typeof this.on_auth === "function") this.on_auth(found);
              }
            } else {
              res.send("EMAIL_NOT_CONFIRMED");
            }
          } else {
            await this.table.update({
              attempts: found.attempts+1
            }, "email = $1", [ found.email ]);
            if (found.attempts+1 >= this.max_attempts) {
              if (!found.security_hash) {
                let new_hash = crypto.randomBytes(32).toString('hex').slice(0, 32);
                await this.table.update({
                  security_hash: new_hash
                }, "email = $1", [ found.email ]);

                let mailOptions = {
                  from: this.auth_code_msg.from,
                  to: found.email,
                  subject: this.auth_code_msg.subject,
                  text: nunjucks.renderString(this.auth_code_msg.text, { code: new_hash }),
                  html: nunjucks.renderString(this.auth_code_msg.html, { code: new_hash })
                };


                await this.mail_session.smtp.send_email(mailOptions);
              }
              respond("SECURITY_CODE_REQUIRED", random_int(default_cfg.auth_timeout.min, default_cfg.auth_timeout.max));
            } else if (!totp_state) {
              respond("TOTP_TOKEN_REQUIRED", random_int(default_cfg.auth_timeout.min, default_cfg.auth_timeout.max));
            } else {
              respond("INVALID_CREDENTIALS", random_int(default_cfg.auth_timeout.min, default_cfg.auth_timeout.max));
            }
          }
        } else {
          // MIMIC USER AUTHENTICATION
          let fake_accs = await this.fake_table.select("*", "email = $1", [data.email.toLowerCase()]);
          if (fake_accs.length > 0) {
            let fake_acc = fake_accs[0];
            if (fake_acc.attempts >= this.max_attempts) {
              respond("SECURITY_CODE_REQUIRED", random_int(default_cfg.auth_timeout.min_fake, default_cfg.auth_timeout.max_fake));
            } else {
              await this.fake_table.update({
                attempts: fake_acc.attempts+1
              }, "email = $1", [ fake_acc.email ]);
              respond("INVALID_CREDENTIALS", random_int(default_cfg.auth_timeout.min_fake, default_cfg.auth_timeout.max_fake));
            }
          } else {
            await this.fake_table.insert({
              email: data.email.toLowerCase(),
              attempts: 1
            });
            respond("INVALID_CREDENTIALS", random_int(default_cfg.auth_timeout.min_fake, default_cfg.auth_timeout.max_fake));
          }
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
}
