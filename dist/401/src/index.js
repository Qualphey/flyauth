
const XHR = require("core/utils/xhr_async");

const fingerprint = require("core/flyauth.ui/fingerprint/index.js");
const pako = require('pako');

const RSA = require("core/flyauth.ui/index.js").RSA;


const crypto = require("crypto");

const Flyauth = require("core/flyauth.ui/index.js");


(async function() {
  try {
    await Flyauth.prepare(false, XHR);

    let credentials = {
      fp: await fingerprint.get(),
      csrf: localStorage.getItem(FLYAUTH_PREFIX+"_csrf_token").slice(0,128),
    };

//    console.log(FLYAUTH_PREFIX+"_csrf_token");
//    console.log(credentials);

    let encreds = await RSA.encrypt(JSON.stringify(credentials), XHR);

//    console.log(encreds.length, encreds);
    let cgz = encodeURIComponent(Buffer.from(pako.deflate(Uint8Array.from(Buffer.from(encreds, 'hex')))).toString("base64"));
//    console.log("CREDENTIALS (LENGTH):", cgz.length);

//    let cgz = encodeURIComponent(btoa(pako.deflate(encreds, { to: 'string' })));
//    console.log("CREDENTIALS (LENGTH):", cgz.length);
//    console.log(cgz);


    let PROTECTED_URL = XHR.getParamByName("original_url");

//    console.log(PROTECTED_URL);

    console.log(PROTECTED_URL+(PROTECTED_URL.includes("?") ? "&" : "?")+"authentials="+cgz);

    if (typeof CORDOVA_APP !== "undefined") {
      app_window.postMessage({
        command: "navigate",
        where:PROTECTED_URL+(PROTECTED_URL.includes("?") ? "&" : "?")+"authentials="+cgz 
      }, "*");
    } else {
      window.location.href = PROTECTED_URL+(PROTECTED_URL.includes("?") ? "&" : "?")+"authentials="+cgz;
    }

  } catch (e) {
    console.error(e.stack);
  }
})();
