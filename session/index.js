
const jwt = require("jsonwebtoken");

module.exports = class {
  static async construct(auth, aura, cfg) {
    try {
      let _this = new module.exports(auth, cfg);

      _this.table = await aura.table(cfg.table_name+"_sessions" || "user_accounts_sessions", {
        columns: {
          id: 'uuid PRIMARY KEY DEFAULT gen_random_uuid()',
          userid: "UUID REFERENCES "+cfg.users_table_name+"(id)",
          jwt_token: 'TEXT',
          fingerprint: "jsonb",
          exp: "bigint"
        }
      });
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(auth, cfg) {
    this.auth = auth;
    
    this.token_expiration_time = cfg.token_exp;
    this.token_expiration_delay = cfg.token_expd;
  }

  async renew_token(req, res) {
    try {
      let sess = req.session_data;

      let _this = this;

      let body = req.body;
      let data = undefined;
      if (body.data) {
        if (typeof body.data === 'string' || body.data instanceof String) {
          try {
            data = JSON.parse(body.data);
          } catch(jsone) {
            data = JSON.parse(_this.rsa.decrypt(body.data));
          }
        }
      }


      let token_expiration = this.token_expiration_time;
      let token_expiration_delay = this.token_expiration_delay;
      let time_to_check = Math.floor(Date.now() / 1000);
//      console.log(sess.exp - token_expiration_delay, time_to_check);
      if (sess.exp - token_expiration_delay <= time_to_check) {
        let ntoken = await this.new_token({
          id: sess.id,
          password: sess.password,
          jwt_token: sess.jwt_token
//          session: sess
        }, res, sess.fingerprint);
        res.json({
          name: this.admin_mode ? "access_token" : this.name+'_access_token',
          data: AES.encrypt(ntoken, data.aes_key),
          exp: token_expiration,
          exp_delay: token_expiration_delay
        });
      } else {
        res.json({
          exp: sess.exp - time_to_check - token_expiration_delay,
          exp_delay: token_expiration_delay
        });
      }

    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }


  async new_token(usr_data, res, fingerprint) {
    try {
      console.log("FlyAuth new token!");
      let csrf_token = crypto.randomBytes(64).toString('hex');
      let token_expiration = this.token_expiration_time;
      let token_expiration_delay = this.token_expiration_delay;

      let jwt_token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + token_expiration + token_expiration_delay,
        id: usr_data.id,
        csrf: csrf_token,
        fingerprint: fingerprint
      }, usr_data.password);
/*
      let existing_sessions = await this.session_table.select("*", "userid = $1", [usr_data.id]);
      let existing_session = undefined;
      for (let s = 0; s < existing_sessions.length; s++) {
        if (JSON.stringify(existing_sessions[s].fingerprint) == JSON.stringify(fingerprint)) {
          existing_session = existing_sessions[s];
        }
      }

      if (existing_session) {
        await this.session_table.update({
          jwt_token: jwt_token,
     //     fingerprint: JSON.stringify(fingerprint)
        }, "id = $1", [existing_session.id]);
      } else {*/
        await this.table.insert({
          jwt_token: jwt_token,
          fingerprint: JSON.stringify(fingerprint),
          userid: usr_data.id,
          exp: Date.now() + (token_expiration + token_expiration_delay) * 1000
        });
//      }

      await this.table.update({
        security_hash: "",
        attempts: 0
      }, "id = $1", [usr_data.id]);

      if (usr_data.jwt_token) {
        res.cookie(this.name+'_access_token_exp', usr_data.jwt_token, {
          httpOnly: true,
          maxAge: 1000 * token_expiration_delay
        });
      }

      res.cookie(this.name+'_access_token', jwt_token, {
        httpOnly: true,
        maxAge: 1000 * (token_expiration + token_expiration_delay)
      });

      return csrf_token;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async create_session(usr_data, res, fingerprint) {
    try {
      let csrf_token = await this.new_token(usr_data, res, fingerprint);

      let next_url = usr_data.next_url;
      if (!next_url && this.paths) {
        next_url = this.paths.authenticated;
      }
      if (usr_data.cfg) {
        if (usr_data.cfg.auth_next)  next_url = usr_data.cfg.auth_next;
      }

/*      console.log("SESSIONG CREATED", {
        csrf_token: csrf_token,
        next_url: next_url
      });*/
      return {
        csrf_token: csrf_token,
        next_url: next_url
      };

    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async decrypt_token(jwt_token) {
    try {
      console.log(">>>>>>>>>>>> DECRYPT TOKEN NEW IMPLEMENTATION");
      if (!jwt_token) {
        console.log("Error: can't decrypt JWT_TOKEN :", jwt_token);
        return undefined;
      }
      var sess = await this.table.select(
        "*", "jwt_token = $1", [jwt_token]
      );
      sess = sess && sess.length > 0 ? sess[0] : undefined;

      var usr = sess ? await this.auth.table.select(
        "*",
        "id = $1", [sess.userid]
      ) : undefined;
      usr = usr && usr.length > 0 ? usr[0] : undefined;

      if (!usr || !sess) {
        return undefined;
      }

      let jwt_payload = undefined;
      try {
        jwt_payload = jwt.verify(jwt_token, usr.password);
      } catch (e) {
        console.error(e);
        return undefined;
      }

      jwt_payload.session_id = sess.id;
      jwt_payload.super = usr.super;
      jwt_payload.cfg = usr.cfg ? usr.cfg : {};
      jwt_payload.password = usr.password;
      if (usr.points) jwt_payload.points = usr.points;
      jwt_payload.fingerprint = sess.fingerprint;
      jwt_payload.jwt_token = sess.jwt_token;

      return jwt_payload;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  check_rights(session_data, required_rights) {
//    console.log("CKRIGHTS", session_data.super, session_data.rights, required_rights);
    if (required_rights) {
      var access_granted = true;
      for (var r = 0; r < required_rights.length; r++) {
        var required_right = required_rights[r];
 //       console.log("RIGHT REQ", required_right);
 //       console.log(session_data);

        if (required_right === 'super_admin') {
          if (!session_data.super) {
            access_granted = false;
            break;
          } else if (this.super_disabled) {
            access_granted = false;
            break;
          }
        } else {
          if (!session_data.rights) session_data.rights = session_data.cfg.rights;
          if (!session_data.rights || !session_data.rights.includes(required_right)) {
            access_granted = false;
            break;
          }
        }
      }

      if (access_granted || session_data.super) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }


  async terminate(req, res, next, skip_res) {
    try {
      let access_token = req.cookies[this.name+'_access_token']
      if (!req.session_data && access_token) req.session_data = await this.decrypt_token(access_token);
//      console.log("SESS", req.session_data);
      if (req.session_data) await this.table.delete("id = $1", [req.session_data.session_id]);

      if (access_token) {
        res.clearCookie(this.name+'_access_token');
        if (!skip_res) res.redirect(this.paths.unauthorized);
      } else {
        if (!skip_res) res.redirect(this.paths.unauthorized);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async details(req, res, next) {
    try {
      if (req.headers.cookie) {
        var cookies = cookie.parse(req.headers.cookie);
        if (cookies[this.name+'_access_token']) {
          req.access_token = cookies[this.name+'_access_token'];
        } else {
          res.redirect(redirect_path);
        }

        var access_token = req.jwt_token;

        if (access_token) {
          var found = await this.table.select(
            '*',
            "jwt_token = $1", [access_token]
          );


          if (found.length > 0) {
            found = found[0];
            delete found.password;
            delete found.jwt_secret;
            delete found.access_token;
            delete found.secret;
            res.send(JSON.stringify(found));
          } else {
            await this.terminate(req, res, next);
          }
        }
      } else {
        res.redirect(redirect_path);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
}
