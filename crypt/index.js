
const crypto = require("crypto");

module.exports = class {
  static AES = class {
    static encrypt(data, key) {
  //    console.log(key);
      key = Buffer.from(key, "base64");
  //    console.log(key.length);

      const iv = crypto.randomBytes(16);
      const cipher = crypto.createCipheriv('aes-256-gcm', key, iv);

      let encrypted = cipher.update(data, 'utf8', 'base64');
      encrypted += cipher.final('base64');
      return iv.toString("base64")+cipher.getAuthTag().toString("base64")+encrypted;
    }
  }

  static RSA = class {
    #rsa_expiration = 1000 * 60 * 60 * 24 * 1;
    #rsa_renew_delay = 1000 * 60 * 60;

    constructor(debug_prefix) {
      this.usr_prefix = debug_prefix;
      this.keys = [];
      this.generate_rsa_keys();
      let _this = this;
      setInterval(function() {
        _this.generate_rsa_keys();
      }, this.#rsa_expiration);
    }

    generate_rsa_keys() {
      let _this = this;
      if (!this.keys) this.keys = [];
      if (this.keys.length > 0) {
        setTimeout(function() {
          _this.keys.shift();
        }, this.#rsa_renew_delay);
      }

      let pass = crypto.randomBytes(64).toString("hex");
      let keys = crypto.generateKeyPairSync('rsa', {
        modulusLength: 2048,
        publicKeyEncoding: {
          type: 'spki',
          format: 'pem'
        },
        privateKeyEncoding: {
          type: 'pkcs8',
          format: 'pem',
          cipher: 'aes-256-cbc',
          passphrase: pass 
        }
      });
      keys.pass = pass;
      keys.id = crypto.randomBytes(32).toString("hex");
      keys.exp = Date.now()+this.#rsa_expiration;
      this.keys.push(keys);

  /*    for (let i = 0; i < this.keys.length; i++) {
        console.log(this.usr_prefix, "UPDATED", this.keys[i].id);
      }*/
    }

    get_public_key() {
      let keys = JSON.parse(JSON.stringify(this.keys[ this.keys.length - 1 ]));
      delete keys.pass;
      delete keys.privateKey;
      keys.exp -= Date.now();
      keys.exp_delay = this.#rsa_renew_delay;
      return keys;
    }

    decrypt(hex_data) {
      let key_id = hex_data.substring(0, 64);
      let data = Uint8Array.from(Buffer.from(hex_data.substring(64), "hex"));

 //     console.log("keyid", key_id);

      let keys = undefined;

      for (let k = 0; k < this.keys.length; k++) {
        if (this.keys[k].id == key_id) {
          keys = this.keys[k];
          break;
        }
      }

      if (!keys) {
        console.error(new Error("INVALID RSA KEY ID!"));
        return undefined;
      } else {
        let decrypted_data = "";

        let chunks = [];

        let array = data;
        var i,j,temparray,chunk = 256;
        for (i=0,j=array.length; i<j; i+=chunk) {
          chunks.push(array.slice(i,i+chunk));
        }

        for (let c = 0; c < chunks.length; c++) {
          try {
            decrypted_data += crypto.privateDecrypt({
              key: keys.privateKey,
              passphrase: keys.pass
            }, chunks[c]).toString("utf8");
          } catch (de) {
            console.debug("CHUNK LENGTH:", chunks[c].length);
            console.debug("privKey:", keys.privateKey);
            console.debug("pass:", keys.pass);
            console.error(de.stack);
          }
        }



        console.log(decrypted_data);
        
        return decrypted_data;
      }
    }
  }
}


