


module.exports = class {
  static async construct() {
    try {
      let _this = new module.exports();
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor() {

  }


  async signup_email(data) {
    try {
      let usr = await this.table.select("*", "email = $1", [data.email]);
      let sgn = await this.signed_email_table.select("*", "email = $1", [data.email]);
      if (usr.length == 0 && sgn.length == 0) {
        let new_hash = crypto.randomBytes(16).toString('hex');
        let sgnid = await this.signed_email_table.insert({
          email: data.email,
          security_hash: new_hash,
          expires: Date.now() + default_cfg.signed_email_expiration
        });
        await this.send_mail(data.email, this.email_code_msg, { code: new_hash+encodeURIComponent(Buffer.from(sgnid).toString('base64')) });
      }

      await asleep(random_int(default_cfg.signup_timeout.min, default_cfg.signup_timeout.max)); 
      return undefined;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async register(data) {
    try {
      let err = false;
      if (!data.email) {
        if (data.verification_id) {
          var ver_email = await this.signed_email_table.select(
            "*",
            "id = $1", 
            [data.verification_id]
          );
          if (ver_email.length > 0) {
            data.email = ver_email[0].email;
          } else {
            err = true;
          }
        } else {
          err = true;
        }
      }
      if (!validate_email(data.email)) err = true;
      if (!data.code) err = true;
      if (!data.pwd) err = true;

      if (this.required_custom_columns) {
        for (var r = 0; r < this.required_custom_columns.length; r++) {
          const reqcol_name = this.required_custom_columns[r];
          if (!data[reqcol_name]) {
            err = true;
          }
        }
      }

      let values_in_use = [];

      if (this.unique_custom_columns) {
        for (var r = 0; r < this.unique_custom_columns.length; r++) {
          const unicol_name = this.unique_custom_columns[r];

          var found_unicol = await this.table.select(
            [unicol_name],
            "("+unicol_name+" = $1)", [data[unicol_name]]
          );
          if (found_unicol.length > 0) {
            err = true;
          }
        }
      }

      let result = {};

      var found_email = await this.signed_email_table.select(
        "*",
        "email = $1 AND security_hash = $2", 
        [data.email, data.code]
      );

      if (!err && found_email.length > 0) {
        var salt = bcrypt.genSaltSync(10);
        var acc_data = {
          email: data.email,

          password: bcrypt.hashSync(data.pwd, salt),
          cfg: {
            rights: ["paskyra"]
          },
          secret: crypto.randomBytes(32).toString('hex'),
          points: 0
        }

        for (let key in this.custom_columns) {
          if (data[key]) {
            acc_data[key] = data[key];  
          }
        }


        if (this.rights) {
          acc_data.super = (data.super == true);
          acc_data.creator = (data.creator == true);
        }

        let undefined_cols = [];
        if (this.required_custom_columns) {
          for (var r = 0; r < this.required_custom_columns.length; r++) {
            const reqcol_name = this.required_custom_columns[r];
            acc_data[reqcol_name] = data[reqcol_name];
          }
        }

        if (this.autolock) {
          acc_data.locked = true;
        }
        
        result.success = true;
        result.id = await this.table.insert(acc_data);

        await this.signed_email_table.delete(
          "email = $1", 
          [data.email]
        );

        result = "SUCCESS";
      } else {
        result = "ERROR";
      }

      await asleep(random_int(default_cfg.signup_timeout.min, default_cfg.signup_timeout.max));
      return JSON.stringify(result);
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  async confirm(code, res) {
    try {
      if (code) {
        let found = await this.table.select(
          ["vardas", "pavarde", "imone", "planas","pareigos", "email", "id", "password"],
          "secret = $1",
          [code]
        );

        if (1 < found.length) {
          console.log("EMAIL CONFIRMATION DUPLICATE << !IMPORTANT");
        }

        if (0 < found.length) {
          var cuser = found[0];
          await this.table.update({
            email_confirmation: true
          }, "email = $1", [cuser.email]);
          let sesres = await this.create_session(cuser, res);

          delete cuser.password;


          if (this.forward_token) {
            res.send(nunjucks_env.render('forward_token.html', {
              user_data: cuser,
              target: this.paths.authenticated,
              access_token: sesres.csrf_token
            }));
          } else {
            res.send(JSON.stringify({
              user_data: cuser,
              access_token: sesres.csrf_token,
              next_url: sesres.next_url
            }));
          }
        } else {
          res.send(JSON.stringify({ err: true }));
        }
      } else {
        res.send(JSON.stringify({ err: true }));
      }
    } catch (e) {
      console.error(e);
    };
  }
}
