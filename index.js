'user strict'

//const Registration = require("./registration/index.js");
//const Authentication = require("./authentication/index.js");
const Authorization = require("./authorization/index.js");
const Session = require("./session/index.js");

var jwt = require("jsonwebtoken");

var cookie = require('cookie');


var pako = require('pako');

const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const cryptorithm = 'aes-256-cbc';

function encrypt(text, cryptword) {
  var cipher = crypto.createCipher(cryptorithm, cryptword)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}

function decrypt(text, cryptword) {
  var decipher = crypto.createDecipher(cryptorithm, cryptword)
  var dec = decipher.update(text, 'hex', 'utf8')
  dec += decipher.final('utf8');
  return dec;
}

const fs = require('fs-extra')
const path = require('path');

const nunjucks = require('nunjucks');
var nunjucks_env = new nunjucks.Environment(new nunjucks.FileSystemLoader([
  __dirname
], {
  autoescape: true,
//      watch: true,
  noCache: true
}));

const schedule = require('node-schedule');

const nodemailer = require('nodemailer');

const default_cfg = {
  token_expiration: 60 * 60, // secs * mins * hrs * days
  token_renew_delay: 60 * 60 * 24 * 3,
  session_exp_check_interval: 1000 * 60 * 10,
  auth_timeout: {
    min_fake: 100,
    max_fake: 3000,
    min: 0,
    max: 2900
  },
  signup_timeout: {
    min: 0,
    max: 3000
  },
  max_auth_attempts: 30,
  signed_email_expiration: 1000 * 60 * 60
}

function obj_to_qstr(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
        v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) : k + "=" + v);
    }
  }
  return str.join("&");
}

function random_int(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const { Authenticator } = require('@otplib/core');
const { createDigest, createRandomBytes } = require('@otplib/plugin-crypto'); // use your chosen crypto plugin
const { keyDecoder, keyEncoder } = require('@otplib/plugin-thirty-two'); // use your chosen base32 plugin

const authenticator = new Authenticator({
  createDigest,
  createRandomBytes,
  keyDecoder,
  keyEncoder
});

async function asleep(ms) {
  try {
    return await new Promise(function(resolve) {
      setTimeout(resolve, ms);
    });
  } catch (e) {
    console.error(e.stack);
    return undefined;
  }
}

function validate_email(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const FlyCrypt = require("./crypt/index.js");
const RSA = FlyCrypt.RSA;
const AES = FlyCrypt.AES;

module.exports = class {

  static async init(app, mail_session, aura, cfg) {
    try {
      if (!cfg) cfg = {};
      if (!cfg.prefix) cfg.prefix = "user";
//      console.log("\x1b[34m"+"FLYAUTH: "+cfg.prefix+"\x1b[0m");

      //      this.mailbot_user_id = cfg.mailbot_user_id;


      let columns = {
        id: 'uuid PRIMARY KEY DEFAULT gen_random_uuid()',
        email: 'varchar(256)',
        password: 'varchar(256)',
        secret: 'varchar(64)',
        cfg: 'jsonb',
        email_confirmation: 'boolean',
        email_confirm_until: 'bigint',
        locked: 'boolean',
        points: "bigint",
        security_hash: 'varchar(32)',
        attempts: "smallint",
        totp_secret: "varchar(16)"
      };

      if (cfg.save_ip_addrs) {
        columns.ip_addrs = 'text[]';
        columns.fingerprints = 'text[]';
      }

      if (cfg.rights) {
        columns.super = 'boolean';
        columns.creator = 'boolean';
      }

      if (cfg.custom_columns) {
        for (var key in cfg.custom_columns) {
          columns[key] = cfg.custom_columns[key];
        }
      }

      let users_table_name = cfg.table_name || "user_accounts";
      var table = await aura.table(users_table_name, {
        columns: columns
      });

      let signed_email_table = await aura.table(cfg.table_name+"_signed_emails" || "user_accounts_signed_emails", {
        columns: {
          id: 'uuid PRIMARY KEY DEFAULT gen_random_uuid()',
          email: "varchar(256)",
          security_hash: 'varchar(32)',
          expires: 'bigint'
        }
      });

      let fake_table = await aura.table(cfg.table_name+"_fakes" || "user_accounts_fakes", {
        columns: {
          id: 'uuid PRIMARY KEY DEFAULT gen_random_uuid()',
          email: 'varchar(256)',
          attempts: "smallint"
        }
      });

/*
      if (cfg.smtp) {
        let transporter = undefined;
        if (cfg.smtp.gmail) {
          transporter = nodemailer.createTransport(cfg.smtp.gmail);
        } else {
          transporter = nodemailer.createTransport({
            host: cfg.smtp.host,
            port: cfg.smtp.port,
            secure: false
          });
        }
        cfg.smtp = await new Promise(function(resolve) {
          function timeout() {
            console.error("SMTP connection timeout!");
            resolve(transporter);
          }

          let timeout_ms = cfg.smtp.timeout || 5000;

          const timeout_id = setTimeout(timeout, timeout_ms);
          transporter.verify(function(error, success) {
            if (error) {
              console.log(error);
            } else {
              clearTimeout(timeout_id);
              resolve(transporter);
            }
          });
        });
      }*/
      let _this = new module.exports(app, mail_session, table, signed_email_table, fake_table, cfg);

      _this.save_ip_addrs = cfg.save_ip_addrs;

      // AUTHORIZATION
      _this.orization = await Authorization.construct(_this, aura, cfg);
      _this.orize = function(req, res, next) {
        _this.orization.authorize(req, res, next)
      }

      _this.orize_gen = function(required_rights) {
        return function(req, res, next) {
          _this.orization.authorize(req, res, next, required_rights)
        }
      }

      _this.use_session = function(req, res, next) {
        _this.orization.attach_session(req, res, next);
      }

      let sess_cfg = {...cfg};
      sess_cfg.users_table_name = users_table_name;
      sess_cfg.token_exp = default_cfg.token_expiration;
      sess_cfg.token_expd = default_cfg.token_renew_delay;

      _this.session = await Session.construct(_this, aura, sess_cfg);

      _this.init_controls();

      setInterval(async function() {
        try {
          await _this.session.table.delete("exp < $1", [Date.now()]);
        } catch (e) {
          console.error(e.stack);
        }
      }, default_cfg.session_exp_check_interval);


      return _this;
    } catch (e) {
      console.error(e.stack);
      return null;
    }
  }

  constructor(app, mail_session, table, signed_email_table, fake_table, cfg) {
    this.rsa = new RSA(cfg.prefix);

    this.token_expiration_time = cfg.token_expiration || default_cfg.token_expiration;
    this.token_expiration_delay = cfg.token_expiration_delay || default_cfg.token_renew_delay;

    this.mail_session = mail_session;
    this.mailbot_user_id = cfg.mailbot_user_id;
    this.prefix = cfg.prefix;

//    console.debug("FLYAUTH_PREFIX", this.prefix);
    this.orize_globals_head = fs.readFileSync(path.resolve(__dirname, "inject", "head-globals.njk"), "utf8");
    this.orize_urls_head = fs.readFileSync(path.resolve(__dirname, "inject", "head.njk"), "utf8");
    this.orize_urls_body = fs.readFileSync(path.resolve(__dirname, "inject", "body.njk"), "utf8");

    this.lang = JSON.parse(fs.readFileSync(
      path.resolve(__dirname, "lang", (cfg.lang || "en")+".json"),
      "utf8"
    ));


    var this_class = this;
    let _this = this;

    this.fake_table = fake_table;

    this.on_auth = cfg.on_auth;

    this.cfg = cfg;

    this.app = app;

    this.max_attempts = default_cfg.max_auth_attempts;

    this.name = cfg.table_name || "user_accounts";
    this.table = table;
    this.signed_email_table = signed_email_table;
    if (!cfg.disable_registration) {
      setInterval(async function() {
        try {
          let signemails = _this.signed_email_table.select("*");
          for (let e = 0; e < signemails.length; e++) {
            if (signemails[e].expires < Date.now()) {
              _this.signed_email_table.delete("email = $1", [signemails[e].email]);
            }
          }
        } catch (e) {
          console.error(e.stack);
        }
      }, 1000 * 60 * 10);
    }

//    this.session_table = session_table;


    this.paths = cfg.auth_paths;

    this.custom_columns = cfg.custom_columns;
    this.required_custom_columns = cfg.required_custom_columns;
    this.unique_custom_columns = cfg.unique_custom_columns;

    this.super_disabled = cfg.super_disabled;

    this.autolock = cfg.autolock;

    this.extra_auth = cfg.extra_auth;
    
    this.admin_mode = cfg.admin_mode;

    if (cfg.mailbot) {
      this.auth_code_msg = cfg.mailbot.auth_code_msg;
      this.email_code_msg = cfg.mailbot.email_code_msg;
      this.msg = cfg.mailbot.msg;
      this.reset_msg = cfg.mailbot.reset_msg;
    }

    this.rights = cfg.rights;


    var this_class = this;


    this.orize_socket = function(socket, next) {
      console.log("SOCKET AUTH");
      let req = {
        headers: socket.request.headers,
        method: "POST",
        body: {},
        get: function(header_name) {
          return socket.request.headers[header_name]
        }
      }
      console.log("req", req);
      let res = {
        status: function(scode) {
          this.scode = scode;
          return this;
        },
        send: function(msg) {
          let err  = new Error(this.scode +" "+ msg);
          err.data = {
            status: this.scode,
            msg: msg
          };

          next(err);
        }
      }
      let nnext = function() {
        socket.session_data = req.session_data;
        next();
      }
      this_class.orization.authorize(req, res, nnext)
    }

    if (app) {
      app.use(function(req, res, next) {
        if (req.headers.cookie) {
          var cookies = cookie.parse(req.headers.cookie);
          if (cookies[this_class.name+'_access_token']) {
            if (!req.access_tokens) req.access_tokens = {};
            req.access_tokens[this_class.name] = cookies[this_class.name+'_access_token'];
          }
        }
        next();
      });
    }




    schedule.scheduleJob('0 0 * * *', async function() {
      const list = await this_class.table.select(["id", "email_confirm_until"], "email_confirmation != $1", [true]);
      for (var a = 0; a < list.length; a++) {
        let acc = list[a];
        if (acc.email_confirm_until < Date.now()) {
          this_class.table.delete("id = $1", [acc.id]);
        }
      }
    });
  }

  init_controls() {
    let app = this.app;
    
    let path_prefix = "/"+this.prefix;
    var app_path = this.app_path = path_prefix+'-auth.io';

    let _this = this;
    let this_class = this;
    let cfg = this.cfg;
//    console.log("FLYAUTH PATH", app_path);
   //     console.log(app_path);
    if (app) {
      app.post(app_path, async function(req, res, next) {
        try {
          var data = req.body;
          
          if (data.data) {
            if (typeof data.data === 'string' || data.data instanceof String) {
              try {
                data = JSON.parse(data.data);
              } catch (jsone) {
                data = JSON.parse(_this.rsa.decrypt(data.data));
              }
            }
          }

          switch (data.command) {
            case 'signup_email':
              if (!cfg.admin_mode && !cfg.disable_registration && !cfg.invitation_only) {
                await _this.signup_email(data);
                res.send("OK");
              }
              break;
            case 'register':
              if (!cfg.admin_mode && !cfg.disable_registration) {
                const result = await this_class.register(data);

                if (result.err) {
                  res.send(result);
                } else {
                  res.send(result);
                //  console.log("REDIRECT", this_class.paths.unauthorized);
                //  res.redirect(this_class.paths.unauthorized);
                }
              }
              break;
            case 'confirm':
              if (!cfg.admin_mode && !cfg.disable_registration) {
                await this_class.confirm(data.code, res);
              }
              break;
            case 'authenticate':
              this_class.authenticate(data, req, res, next);
              break;
            case 'terminate':
              await this_class.terminate(req, res, next);
              break;
            case 'reset-pwd':
              if (!cfg.admin_mode) {
                await this_class.reset_pwd(data, req, res, next);
              }
              break;
            default:
              console.error(new Error("FLYAUTH ERROR: `"+app_path+"` INVALID COMMAND: "+data.command));
          }
        } catch (e) {
          console.error(e.stack);
        }
      });

      app.post("/"+this.prefix+'-user_session', this.orize, async function(req, res, next) {
        try {
          let sessions = await this_class.session.table.select("*", "userid = $1", [req.session_data.id]);
          let sess = undefined;
          if (sessions.length > 0 && req.headers.fingerprint) {
            for (let s = 0; s < sessions.length; s++) {
              if (JSON.stringify(sessions[s].fingerprint) == JSON.stringify(req.headers.fingerprint)) {
                sess = sessions[s];
              }
            }
          }

          if (!sess) res.sendStatus(401);

          let usr = (await this_class.table.select("*", "id = $1", [sess.userid]))[0];
          usr.jwt_token = await this_class.decrypt_token(sess.jwt_token);
          if (usr.jwt_token) delete usr.jwt_token.csrf;
          delete usr.password;
          res.json(usr);
        } catch (e) {
          console.error(e.stack);
        }
      });

      var restricted_path = "/"+this.prefix+'-auth.io-restricted';
      app.post(restricted_path, this.orize, async function(req, res, next) {
        try {
          var data = req.body;

          if (data.data) {
            if (typeof data.data === 'string' || data.data instanceof String) {
              try {
                data = JSON.parse(data.data);
              } catch (jsone) {
                data = JSON.parse(_this.rsa.decrypt(data.data));
              }
            }
          }

          switch (data.command) {
            case 'details':
              this_class.details(req, res, next);
              break;
            case 'edit':
              this_class.edit(data, res);
              break;
            case 'renew-token':
              await this_class.renew_token(req, res);
              break;
            case 'cpwd':
              await this_class.cpwd(data, res);
              break;
            default:
          }
        } catch (e) {
          console.error(e.stack);
        }
      });

      app.get(app_path, async function(req, res, next) {
          var data = req.query;
          
        console.log(data);
          if (data.data) {
            if (typeof data.data === 'string' || data.data instanceof String) {
              data = JSON.parse(data.data);
            }
          }

          switch (data.command) {
            case 'get_public_key':
              console.log("get_public_key");
              res.json(_this.rsa.get_public_key());
              break;
            case 'get_configuration':
              res.json({
                token_expiration_time: _this.token_expiration_time,
                token_expiration_delay: _this.token_expiration_delay
              });
              break;
            default:
              res.json({ err: "INVALID COMMAND" });
          }
      //  await this_class.terminate(req, res, next);
      });
    }
  }

  async send_mail(to, msg, args) {
    try {
      let mailOptions = {
        from: msg.from,
        to: to,
        subject: msg.subject,
        text: nunjucks.renderString(msg.text, args),
        html: nunjucks.renderString(msg.html, args)
      };

      await this.mail_session.smtp.send_email(mailOptions);

    } catch (e) {
      console.error(e.stack);
    }
  }

  async signup_email(data) {
    try {
      let usr = await this.table.select("*", "email = $1", [data.email]);
      let sgn = await this.signed_email_table.select("*", "email = $1", [data.email]);
      if (usr.length == 0 && sgn.length == 0) {
        let new_hash = crypto.randomBytes(16).toString('hex');
        let sgnid = await this.signed_email_table.insert({
          email: data.email,
          security_hash: new_hash,
          expires: Date.now() + default_cfg.signed_email_expiration
        });
        if (data.invitation) new_hash += encodeURIComponent(Buffer.from(sgnid).toString('base64'));
        await this.send_mail(data.email, this.email_code_msg, { code: new_hash });
      }

      await asleep(random_int(default_cfg.signup_timeout.min, default_cfg.signup_timeout.max)); 
      return undefined;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async register(data) {
    try {
      let err = false;
      if (!data.email) {
        if (data.verification_id) {
          var ver_email = await this.signed_email_table.select(
            "*",
            "id = $1", 
            [data.verification_id]
          );
          if (ver_email.length > 0) {
            data.email = ver_email[0].email;
          } else {
            err = true;
          }
        } else {
          err = true;
        }
      }
      if (!validate_email(data.email)) err = true;
      if (!data.code) err = true;
      if (!data.pwd) err = true;

      if (this.required_custom_columns) {
        for (var r = 0; r < this.required_custom_columns.length; r++) {
          const reqcol_name = this.required_custom_columns[r];
          if (!data[reqcol_name]) {
            err = true;
          }
        }
      }

      let values_in_use = [];

      if (this.unique_custom_columns) {
        for (var r = 0; r < this.unique_custom_columns.length; r++) {
          const unicol_name = this.unique_custom_columns[r];

          var found_unicol = await this.table.select(
            [unicol_name],
            "("+unicol_name+" = $1)", [data[unicol_name]]
          );
          if (found_unicol.length > 0) {
            err = true;
          }
        }
      }

      let result = {};

      var found_email = await this.signed_email_table.select(
        "*",
        "email = $1 AND security_hash = $2", 
        [data.email, data.code]
      );

      if (!err && found_email.length > 0) {
        var salt = bcrypt.genSaltSync(10);
        var acc_data = {
          email: data.email,

          password: bcrypt.hashSync(data.pwd, salt),
          cfg: {
            rights: ["paskyra"]
          },
          secret: crypto.randomBytes(32).toString('hex'),
          points: 0
        }

        for (let key in this.custom_columns) {
          if (data[key]) {
            acc_data[key] = data[key];  
          }
        }


        if (this.rights) {
          acc_data.super = (data.super == true);
          acc_data.creator = (data.creator == true);
        }

        let undefined_cols = [];
        if (this.required_custom_columns) {
          for (var r = 0; r < this.required_custom_columns.length; r++) {
            const reqcol_name = this.required_custom_columns[r];
            acc_data[reqcol_name] = data[reqcol_name];
          }
        }

        if (this.autolock) {
          acc_data.locked = true;
        }
        
        result.success = true;
        result.id = await this.table.insert(acc_data);

        await this.signed_email_table.delete(
          "email = $1", 
          [data.email]
        );

        result = "SUCCESS";
      } else {
        result = "ERROR";
      }

      await asleep(random_int(default_cfg.signup_timeout.min, default_cfg.signup_timeout.max));
      return JSON.stringify(result);
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  async confirm(code, res) {
    try {
      if (code) {
        let found = await this.table.select(
          ["vardas", "pavarde", "imone", "planas","pareigos", "email", "id", "password"],
          "secret = $1",
          [code]
        );

        if (1 < found.length) {
          console.log("EMAIL CONFIRMATION DUPLICATE << !IMPORTANT");
        }

        if (0 < found.length) {
          var cuser = found[0];
          await this.table.update({
            email_confirmation: true
          }, "email = $1", [cuser.email]);
          let sesres = await this.create_session(cuser, res);

          delete cuser.password;


          if (this.forward_token) {
            res.send(nunjucks_env.render('forward_token.html', {
              user_data: cuser,
              target: this.paths.authenticated,
              access_token: sesres.csrf_token
            }));
          } else {
            res.send(JSON.stringify({
              user_data: cuser,
              access_token: sesres.csrf_token,
              next_url: sesres.next_url
            }));
          }
        } else {
          res.send(JSON.stringify({ err: true }));
        }
      } else {
        res.send(JSON.stringify({ err: true }));
      }
    } catch (e) {
      console.error(e);
    };
  }

  async authenticate(data, req, res, next) {
    var this_class = this;

    function respond(msg, ms) {
      setTimeout(function() {
        res.send(msg);
      }, ms);
    }

    try {
      const ip_addr = req.headers['x-forwarded-for'] || req.socket.remoteAddress;

      if (data.fp) {
        let fpmod = JSON.parse(data.fp);
        fpmod.push(req.get("user-agent"));
        fpmod.push(ip_addr);
        data.fp = JSON.stringify(fpmod);
      }

      if (!data.email || !data.pwd) {
        res.send(JSON.stringify({
          err: true
        }));
      } else {
        let found = await this.table.select(
          '*', "(email = $1)", [data.email.toLowerCase()]
        );

        if (0 < found.length) {
          found = JSON.parse(JSON.stringify(found[0]));

          let totp_state = this.totp_confirm(found, data);
          console.log("FLYAUTH attempt:", data.email,  (found.attempts < this.max_attempts || data.security_hash == found.security_hash));
          console.log("PASSWORD:", bcrypt.compareSync(data.pwd, found.password));
          console.log("FP", data.fp);
          console.log("TOTP", totp_state);
          if (
            (found.attempts < this.max_attempts || data.security_hash == found.security_hash)
            && bcrypt.compareSync(data.pwd, found.password)
            && data.fp 
            && totp_state
          ) {
            let proceed = true;
         /*   if (this.smtp) {
              if (!found.email_confirmation) {
                proceed = false;
              }
            }*/
            if (proceed) {
              found.next_url = data.next_url;
              let sesres = await this.create_session(found, res, data.fp, ip_addr);
              delete found.password;
              delete found.secret;
              delete found.email_confirmation;
              delete found.security_hash;
              delete found.totp_secret;
              if (this.forward_token) {
                res.json({
                  target: this.paths.authenticated,
                  access_token: sesres.csrf_token
                });
                if (typeof this.on_auth === "function") this.on_auth(found);
              } else {
                res.json({
                  access_token: AES.encrypt(sesres.csrf_token, data.aes_key),
                  next_path: found.super ? "/mellisuga" : this.app.cms.content_manager.path
                });
                if (typeof this.on_auth === "function") this.on_auth(found);
              }
            } else {
              res.send("EMAIL_NOT_CONFIRMED");
            }
          } else {
            await this.table.update({
              attempts: found.attempts+1
            }, "email = $1", [ found.email ]);
            if (found.attempts+1 >= this.max_attempts) {
              if (!found.security_hash) {
                let new_hash = crypto.randomBytes(32).toString('hex').slice(0, 32);
                await this.table.update({
                  security_hash: new_hash
                }, "email = $1", [ found.email ]);

                let mailOptions = {
                  from: this.auth_code_msg.from,
                  to: found.email,
                  subject: this.auth_code_msg.subject,
                  text: nunjucks.renderString(this.auth_code_msg.text, { code: new_hash }),
                  html: nunjucks.renderString(this.auth_code_msg.html, { code: new_hash })
                };


                await this.mail_session.smtp.send_email(mailOptions);
              }
              respond("SECURITY_CODE_REQUIRED", random_int(default_cfg.auth_timeout.min, default_cfg.auth_timeout.max));
            } else if (!totp_state) {
              respond("TOTP_TOKEN_REQUIRED", random_int(default_cfg.auth_timeout.min, default_cfg.auth_timeout.max));
            } else {
              respond("INVALID_CREDENTIALS", random_int(default_cfg.auth_timeout.min, default_cfg.auth_timeout.max));
            }
          }
        } else {
          // MIMIC USER AUTHENTICATION
          let fake_accs = await this.fake_table.select("*", "email = $1", [data.email.toLowerCase()]);
          if (fake_accs.length > 0) {
            let fake_acc = fake_accs[0];
            if (fake_acc.attempts >= this.max_attempts) {
              respond("SECURITY_CODE_REQUIRED", random_int(default_cfg.auth_timeout.min_fake, default_cfg.auth_timeout.max_fake));
            } else {
              await this.fake_table.update({
                attempts: fake_acc.attempts+1
              }, "email = $1", [ fake_acc.email ]);
              respond("INVALID_CREDENTIALS", random_int(default_cfg.auth_timeout.min_fake, default_cfg.auth_timeout.max_fake));
            }
          } else {
            await this.fake_table.insert({
              email: data.email.toLowerCase(),
              attempts: 1
            });
            respond("INVALID_CREDENTIALS", random_int(default_cfg.auth_timeout.min_fake, default_cfg.auth_timeout.max_fake));
          }
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async renew_token(req, res) {
    try {
      let sess = req.session_data;

      let _this = this;

      let body = req.body;
      let data = undefined;
      if (body.data) {
        if (typeof body.data === 'string' || body.data instanceof String) {
          try {
            data = JSON.parse(body.data);
          } catch(jsone) {
            data = JSON.parse(_this.rsa.decrypt(body.data));
          }
        }
      }


      let token_expiration = this.token_expiration_time || default_cfg.token_expiration;
      let token_expiration_delay = this.token_expiration_delay || default_cfg.token_renew_delay;
      let time_to_check = Math.floor(Date.now() / 1000);
//      console.log(sess.exp - token_expiration_delay, time_to_check);
      if (sess.exp - token_expiration_delay <= time_to_check) {
        let ntoken = await this.new_token({
          id: sess.id,
          password: sess.password,
          jwt_token: sess.jwt_token
//          session: sess
        }, res, sess.fingerprint);
        res.json({
          name: this.admin_mode ? "access_token" : this.name+'_access_token',
          data: AES.encrypt(ntoken, data.aes_key),
          exp: token_expiration,
          exp_delay: token_expiration_delay
        });
      } else {
        res.json({
          exp: sess.exp - time_to_check - token_expiration_delay,
          exp_delay: token_expiration_delay
        });
      }

    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }


  async new_token(usr_data, res, fingerprint) {
    try {
      console.log("NEW TOKEN");
      let csrf_token = crypto.randomBytes(64).toString('hex');
      let token_expiration = this.token_expiration_time || default_cfg.token_expiration;
      let token_expiration_delay = this.token_expiration_delay || default_cfg.token_renew_delay;

      let jwt_token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + token_expiration + token_expiration_delay,
        id: usr_data.id,
        csrf: csrf_token,
        fingerprint: fingerprint
      }, usr_data.password);
/*
      let existing_sessions = await this.session_table.select("*", "userid = $1", [usr_data.id]);
      let existing_session = undefined;
      for (let s = 0; s < existing_sessions.length; s++) {
        if (JSON.stringify(existing_sessions[s].fingerprint) == JSON.stringify(fingerprint)) {
          existing_session = existing_sessions[s];
        }
      }

      if (existing_session) {
        await this.session_table.update({
          jwt_token: jwt_token,
     //     fingerprint: JSON.stringify(fingerprint)
        }, "id = $1", [existing_session.id]);
      } else {*/
        await this.session.table.delete("userid = $1 AND fingerprint = $2", [
          usr_data.id,
          JSON.stringify(fingerprint)
        ]);
        await this.session.table.insert({
          jwt_token: jwt_token,
          fingerprint: JSON.stringify(fingerprint),
          userid: usr_data.id,
          exp: Date.now() + (token_expiration + token_expiration_delay) * 1000
        });
//      }

      await this.table.update({
        security_hash: "",
        attempts: 0
      }, "id = $1", [usr_data.id]);

      if (usr_data.jwt_token) {
        res.cookie(this.name+'_access_token_exp', usr_data.jwt_token, {
          httpOnly: true,
          maxAge: 1000 * token_expiration_delay
        });
      }

      res.cookie(this.name+'_access_token', jwt_token, {
        httpOnly: true,
        maxAge: 1000 * (token_expiration + token_expiration_delay)
      });

      return csrf_token;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async create_session(usr_data, res, fingerprint, ip_addr) {
    try {
      let csrf_token = await this.new_token(usr_data, res, fingerprint);

      if (this.save_ip_addrs) {
        let ip_already_saved = false;
        let ip_usrs = await this.table.select(["id"], "ip_addrs @> $1", ['{"'+ip_addr+'"}'])
        for (let i = 0; i < ip_usrs.length; i++) {
          let ip_usr = ip_usrs[i];
          if (ip_usr.id === usr_data.id) ip_already_saved = true;
        }
        if (!ip_already_saved) {
          await this.table.update({
            ip_addrs: "||"+ip_addr
          },  "id = $1", [usr_data.id]);
        }

        let fp_already_saved = false;
        let fp_usrs = await this.table.select(["id"], "fingerprints @> $1", ['{"'+fingerprint.replace(/"/g, "&quot;")+'"}'])
        for (let i = 0; i < fp_usrs.length; i++) {
          let fp_usr = fp_usrs[i];
          if (fp_usr.id === usr_data.id) fp_already_saved = true;
        }

        if (!fp_already_saved) {
          await this.table.update({
            fingerprints: "||"+fingerprint.replace(/"/g, "&quot;")
          },  "id = $1", [usr_data.id]);
        }
      }

      let next_url = usr_data.next_url;
      if (!next_url && this.paths) {
        next_url = this.paths.authenticated;
      }
      if (usr_data.cfg) {
        if (usr_data.cfg.auth_next)  next_url = usr_data.cfg.auth_next;
      }

/*      console.log("SESSIONG CREATED", {
        csrf_token: csrf_token,
        next_url: next_url
      });*/
      return {
        csrf_token: csrf_token,
        next_url: next_url
      };

    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async decrypt_token(jwt_token) {
    try {
      console.log(">>>>>>>>>>>> DECRYPT TOKEN OLD IMPLEMENTATION");
      var sess = await this.session.table.select(
        "*", "jwt_token = $1", [jwt_token]
      );
      sess = sess && sess.length > 0 ? sess[0] : undefined;

      var usr = sess ? await this.table.select(
        "*",
        "id = $1", [sess.userid]
      ) : undefined;
      usr = usr && usr.length > 0 ? usr[0] : undefined;

      if (!usr || !sess) {
        return undefined;
      }

      let jwt_payload = undefined;
      try {
        jwt_payload = jwt.verify(jwt_token, usr.password);
      } catch (e) {
        console.error(e);
        return undefined;
      }

      jwt_payload.session_id = sess.id;
      jwt_payload.super = usr.super;
      jwt_payload.cfg = usr.cfg ? usr.cfg : {};
      jwt_payload.password = usr.password;
      if (usr.points) jwt_payload.points = usr.points;
      jwt_payload.fingerprint = sess.fingerprint;
      jwt_payload.jwt_token = sess.jwt_token;

      return jwt_payload;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  check_rights(session_data, required_rights) {
//    console.log("CKRIGHTS", session_data.super, session_data.rights, required_rights);
    if (required_rights) {
      var access_granted = true;
      for (var r = 0; r < required_rights.length; r++) {
        var required_right = required_rights[r];
 //       console.log("RIGHT REQ", required_right);
 //       console.log(session_data);

        if (required_right === 'super_admin') {
          if (!session_data.super) {
            access_granted = false;
            break;
          } else if (this.super_disabled) {
            access_granted = false;
            break;
          }
        } else {
          if (!session_data.rights) session_data.rights = session_data.cfg.rights;
          if (!session_data.rights || !session_data.rights.includes(required_right)) {
            access_granted = false;
            break;
          }
        }
      }

      if (access_granted || session_data.super) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }


  async terminate(req, res, next, skip_res) {
    try {
      let access_token = req.cookies[this.name+'_access_token']
      if (!req.session_data && access_token) req.session_data = await this.decrypt_token(access_token);
//      console.log("SESS", req.session_data);
      if (req.session_data) await this.session.table.delete("id = $1", [req.session_data.session_id]);

      if (access_token) {
        res.clearCookie(this.name+'_access_token');
      }
      
      if (!skip_res && this.paths) {
        res.redirect(this.paths.unauthorized);
      } else {
        res.send("DONE");
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async details(req, res, next) {
    try {
      if (req.headers.cookie) {
        var cookies = cookie.parse(req.headers.cookie);
        if (cookies[this.name+'_access_token']) {
          req.access_token = cookies[this.name+'_access_token'];
        } else {
          res.redirect(redirect_path);
        }

        var access_token = req.jwt_token;

        if (access_token) {
          var found = await this.table.select(
            '*',
            "jwt_token = $1", [access_token]
          );


          if (found.length > 0) {
            found = found[0];
            delete found.password;
            delete found.jwt_secret;
            delete found.access_token;
            delete found.secret;
            res.send(JSON.stringify(found));
          } else {
            await this.terminate(req, res, next);
          }
        }
      } else {
        res.redirect(redirect_path);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async edit(data, res) {
    try {
      switch (data.what) {
        case "fname":
          await this.table.update({
              vardas: data.value
            },
            "id = $1", [data.id]
          );
          res.send("{}");
          break;
        case "lname":
          await this.table.update({
              pavarde: data.value
            },
            "id = $1", [data.id]
          );
          res.send("{}");
          break;
        case "phone":
          await this.table.update({
              tel_nr: data.value
            },
            "id = $1", [data.id]
          );
          res.send("{}");
          break;
        default:
          console.error("AUTH.IO: invalid edit target: ", data.what);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async reset_pwd(data, req, res, next) {
    try {
     // TODO: determine if email exists database before doing anything...

      const n_secret = crypto.randomBytes(32).toString('hex');

      await this.table.update({
        secret: n_secret
      }, "email = $1", [data.email]);

      const mailOptions = {
        from: this.reset_msg.from,
        to: data.email,
        subject: this.reset_msg.subject,
        text: nunjucks.renderString(this.reset_msg.text, { code: n_secret }),
        html: nunjucks.renderString(this.reset_msg.html, { code: n_secret })

      };

      this.mail_session.smtp.send_email(mailOptions);
      res.send({ success: "success" }); 
    } catch (e) {
      console.error(e.stack);
    }
  }

  async cpwd(data, res) {
    try {
      if (data.pwd === data.pwdr && data.code) {
        var salt = bcrypt.genSaltSync(10);
        let result = await this.table.update({
          password: bcrypt.hashSync(data.pwd, salt),
          secret: ''
        }, "secret = $1", [data.code]);
        
        if (result.length > 0) {
          res.send({
            success: "success"
          });
        } else {
          res.send({
            err: "code already used"
          });
        }
      } else {
        res.send({
          err: "err"
        });
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  failed_response(res, msg) {
    res.send(msg);
  }

  io_parse_token(socket, next) {
    console.log("IO PARSE TOKEN");
    if (socket.request.headers.cookie) {
      var cookies = cookie.parse(socket.request.headers.cookie);
      if (cookies[this.name+'_access_token']) {
        socket.access_token = cookies[this.name+'_access_token'];
      }
    }
    next();
  }

  builtin_pages(pages, cfg) {
    console.log("BUILTIN",this.prefix);
    let page = pages.serve_dirs('/flyauth-'+this.prefix, path.resolve(__dirname, "dist"), {
      name: "auth_builtin",
      globals_path: cfg.globals_path,
      extras: {
        lang_path: path.resolve(__dirname, "lang")
      },
      context: {
        flyauth_prefix: this.prefix
      },
      auth: this
    });
  }

  async get_session(req) {
    try {
      let jwt_token = undefined;
      if (req.headers.cookie) {
        var cookies = cookie.parse(req.headers.cookie);
        if (cookies[this.name+'_access_token']) {
          req.jwt_token = jwt_token = cookies[this.name+'_access_token'];
        }
      }

      let redirect_path = undefined;
      if (req.method === "GET") {
        redirect_path = this.paths.unauthorized+"?next_url="+encodeURIComponent(req.originalUrl);
      }

      if (jwt_token) {
        return await this.decrypt_token(jwt_token);
      } else {
        return false;
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async all_sessions() {
    try {
      let sessions = await this.session.table.select("*");
      let accounts = [];
      for (let s = 0; s < sessions.length; s++) {
        accounts.push(...(await this.table.select("*", "id = $1", [sessions[s].userid])));
      }

      return accounts;
    } catch (e) {
      console.error(e.stack);
    }
  }

  totp_confirm(user, data) {
    if (user.totp_secret) {
      console.log(authenticator.generate(user.totp_secret) === data.totp_token);
      return (authenticator.generate(user.totp_secret) === data.totp_token);
    } else {
      return true;
    }
  }
}
